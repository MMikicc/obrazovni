import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Classroom from './Components/Classroom/Classroom';
import Homepage from './Components/Homepage/Homepage';

const App = () => {
  return (
      <Router>
        <Switch>
          <Route exact path="/" component={Homepage}></Route>
          <Route path="/:id/:className" component={Classroom}></Route>
        </Switch>
      </Router>
  );
}

export default App;
