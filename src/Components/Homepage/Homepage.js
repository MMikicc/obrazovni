import React, { useEffect, useState } from 'react';
import ClassroomCard from './ClassroomCard';

const Homepage = () => {
    const [addClassroomState, toggleAddClassroom] = useState(false);
    const [className, changeClassName] = useState("");
    const [classrooms, setClassrooms] = useState([]);

    useEffect(() => {
        fetch("http://obrazovni.test/api/classroom")
         .then(res => res.json())
         .then(res => setClassrooms(res.data))
    }, [])

    const addClassroom = () => {
        console.log(className);
        fetch("http://obrazovni.test/api/classroom", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
              },
            body: JSON.stringify({name: className})
        })
         .then(res => res.json())
         .then(res => {
            setClassrooms([
                ...classrooms,
                {
                    ...res
                }
            ]);
        })
        toggleAddClassroom(false);
    }

        return (
            <div>
                <div className={`container-fluid ${addClassroomState ? 'muteClassroomBackground' : 'none'}`}>
                    <div className="row">
                        <div className="col-12 text-center text-white h1 p-5">
                            Šta je sortiranje?
                            <div className="h4 p-1">
                            Organizovanje u skladu sa nekim <span className="text-success">šablonom</span> ili <span className="text-success">pravilom</span>. 
                            Sortiranje se obično radi po abecednom redu, mada postoje i numerička sortiranja. 
                            Sortiranje se može raditi po rastućem ili opadajućem redosledu.
                            </div>
                        </div>           
                    </div>
                    <div className="row">
                        {classrooms && classrooms.length > 0 && classrooms.map(classroom => {
                            return <ClassroomCard key={classroom.id} classroom ={classroom}/>
                        })}
                        <div className="col-xl-3 col-md-6 p-1">
                            <div onClick={() => {toggleAddClassroom(true)}} style={{cursor: "pointer"}} className="card border-success shadow h-100 py-1">
                                <div className="card-body pointer">
                                    <div className="row no-gutters align-items-center">
                                        <div className="col-12 text-center mr-2">
                                            <div className="h4 font-weight-bold text-success mb-1">
                                                Dodaj odeljenje <i className="fas fa-plus-circle"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div className={`${addClassroomState? 'addClassroomPopup': 'hideAddStudent'}`}>
                        <div className="container-fluid">
                                    <div className="row pt-2">
                                        <div className="col-6 text-center h2 offset-3 pt-2">
                                            Dodaj odeljenje
                                        </div>
                                        <div className="col-1 offset-2 pt-2">
                                            <h1><i style={arrowStyle} onClick={ () => {toggleAddClassroom(false)}} className="fas fa-times"></i></h1>
                                        </div>
                                    </div>
                                    <div className="row pt-5 col-6 offset-3">
                                        <input style={{textAlign: 'center'}} onChange={(e) => {changeClassName(e.target.value)}} type="text" value={className} className="form-control form-control-user" id="exampleFirstName" placeholder="Naziv odeljenja"/>
                                    </div>
                                    <div className="row pt-5 col-6 offset-3">
                                        <a onClick={() => addClassroom()} className="btn btn-primary btn-user btn-block">
                                                Dodaj odeljenje
                                        </a>
                                    </div> 
                        </div>
                </div>
            </div>
        )      
}

const arrowStyle = {
    cursor: 'pointer'
}

export default Homepage;