import React from 'react';
import { Link } from 'react-router-dom';

const ClassroomCard = (props) => {
    return (
        <div className="col-xl-3 col-md-6 p-1">
            <Link style={{textDecoration: 'none'}} to={`/${props.classroom.id}/${props.classroom.name}`}>
                <div className="card border-left-success shadow h-100 py-1 ">
                    <div className="card-body">
                        <div className="row no-gutters align-items-center">
                            <div className="col mr-2">
                                <div className="h5 font-weight-bold text-success text-uppercase mb-1">
                                    {props.classroom.name}
                                </div>
                            </div>
                            <div className="col-auto">
                                <i className="fab fa-odnoklassniki fa-2x text-gray-500"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    )
}

export default ClassroomCard;