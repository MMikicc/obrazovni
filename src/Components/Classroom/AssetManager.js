import slika1 from './Images/1.png';
import slika2 from './Images/2.png';
import slika3 from './Images/3.png';
import slika4 from './Images/4.png';
import slika5 from './Images/5.png';
import slika6 from './Images/6.png';
import slika7 from './Images/7.png';
import slika8 from './Images/8.png';
import slika9 from './Images/9.png';
import slika10 from './Images/10.png';

export const LoadImages = () => {
    let images = [];
    const image1 = new Image();
    image1.src = slika1;
    const image2 = new Image();
    image2.src = slika2;
    const image3 = new Image();
    image3.src = slika3;
    const image4 = new Image();
    image4.src = slika4;
    const image5 = new Image();
    image5.src = slika5;
    const image6 = new Image();
    image6.src = slika6;
    const image7 = new Image();
    image7.src = slika7;
    const image8 = new Image();
    image8.src = slika8;
    const image9 = new Image();
    image9.src = slika9;
    const image10 = new Image();
    image10.src = slika10;

    images.push(
        {name:'slika1' ,image:image1},
        {name: 'slika2', image:image2},
        {name: 'slika3',image:image3},
        {name: 'slika4',image:image4},
        {name: 'slika5',image:image5},
        {name: 'slika6',image:image6},
        {name: 'slika7',image:image7},
        {name: 'slika8',image:image8},
        {name: 'slika9',image:image9},
        {name: 'slika10',image:image10},
    );
    return images;
}