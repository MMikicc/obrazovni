import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { LoadImages } from './AssetManager';
import Canvas from './Canvas';
import BackgroundImage from './Images/newBackground.jpg';
import * as SORT_INFO from './SortInfo';

const backgroundStyleClass = {
    backgroundImage: `url(${BackgroundImage})`,
    height: '100vh',
}

const arrowDivStyle = {
    alignSelf: 'center'
}

const arrowStyle = {
    cursor: 'pointer'
}

const sortInfoContainer = {
    width: '60%',
    left: '20%', 
    position: 'relative', 
    color: 'white', 
    fontSize: '20px',
}

const sortInfoText = {
    backgroundColor: 'rgb(152, 251, 152, 0.8)',
    border: '2px solid white',
    borderRadius: '5px',
}

const Classroom = () => {
    const { id, className } = useParams();
    const [ students, setStudents ] = useState([]);
    const [ addStudent, toggleAddStudent] = useState(false);
    const [ images, setImages] = useState([]);
    const [ selectedImage, setSelectedImage ] = useState(0);
    const [ ime, setIme ] = useState("");
    const [ prezime, setPrezime ] = useState("");
    const [ visina, setVisina ] = useState(0);
    const [ backgroundStyle, setBackgroundStyle ] = useState(backgroundStyleClass);
    const [ sortInfo, setSortInfo ] = useState('');

    useEffect(() => {
        setImages(LoadImages());
        fetch(`http://obrazovni.test/api/student/${id}`)
         .then(res => res.json())
         .then(res => setStudents(res))
    }, [id])

    const dodajUcenika = () => {
        fetch(`http://obrazovni.test/api/student/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify({
                classroom_id: id,
                firstname: ime,
                lastname: prezime,
                height: visina,
                image: "slika" + (selectedImage + 1),
            }),
        }
        ).then(res => res.json()
        ).then(res => {
            console.log(res);
            setStudents([
                ...students,
                {
                    ...res,
                    height: parseInt(res.height),
                }
            ]);
            toggleAddStudent(false);
        })
    };

    const  shuffleStudents = () => {
        let newStudents = [...students];
        newStudents = newStudents.sort(() => Math.random() - 0.5);
        setStudents(newStudents);
    };

    const disableBackground = (disable = false) => {
        setBackgroundStyle(
            {
                ...backgroundStyleClass,
                pointerEvents: disable ? 'none' : '',
            }
        );
        toggleAddStudent(false);
    };

    const showSortInfo = (sortType) => {
        if (sortType === "Selection") {
            setSortInfo(SORT_INFO.SELECTION_SORT);
        }
        else if (sortType === "Quick") {
            setSortInfo(SORT_INFO.QUICK_SORT);
        }
        else {
            setSortInfo(SORT_INFO.BUBBLE_SORT);
        }
    };

    return (
        <div style={backgroundStyle}>
            <div>
                <div className={`container-fluid ${addStudent? 'muteClassroomBackground': 'none'}`}>
                    <div className="row">
                        <div className="col-2">
                            <div className="text-white h1 text-center pt-5">
                                {className}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2 text-center pt-4 text-black font-weight-bold">
                            {students && students.length > 0 && students.map( (student) => {
                                return <div key={student.id} className="col-10 offset-1 border rounded bg-white p-1 mt-1">{student.firstname} {student.lastname}</div>
                            })}
                        </div>
                        <div className="col-10">
                            <Canvas showSortInfo={showSortInfo} disableBackground={disableBackground} students={students}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2 text-center">
                            <div>
                                <button onClick={() => shuffleStudents()} style={{width: '180px'}} className="btn btn-success">Promesaj ucenike</button>
                            </div>  
                            <div className="pt-3">
                                <div onClick={() => {toggleAddStudent(true)}} style={{width: '180px'}} className="btn btn-success">
                                    <span className="text">Dodaj ucenika</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-8 text-center">
                            <div style={sortInfo !== "" ? sortInfoContainer : {display: 'none'}}>
                                <div style={sortInfoText}>
                                    {sortInfo}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${addStudent? 'addStudentPopUp': 'hideAddStudent'}`}>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-7 offset-3 text-center pt-2">
                                <h1>Dodaj ucenika</h1>
                            </div>
                            <div className="col-1 offset-1 pt-2">
                                <h1><i style={arrowStyle} onClick={ () => {toggleAddStudent(false)}} className="fas fa-times"></i></h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-5">
                                <div className="pt-5">
                                    <input onChange={(e) => setIme(e.target.value)} type="text" className="form-control form-control-user" id="firstName"
                                        placeholder="Ime"/>
                                </div>
                                <div className="pt-3">
                                    <input onChange={(e) => setPrezime(e.target.value)} type="text" className="form-control form-control-user" id="lastName"
                                        placeholder="Prezime"/>
                                </div>
                                <div className="pt-3">
                                    <input onChange={(e) => setVisina(e.target.value)} type="number" className="form-control form-control-user" id="height"
                                        placeholder="Visina"/>
                                </div>
                            </div>
                            <div className="col-1 offset-1" style={arrowDivStyle}>
                                <h2><i style={arrowStyle} onClick={() => setSelectedImage( selectedImage > 0 ? selectedImage - 1 : (selectedImage - 1 < 0 ? images.length - 1 : 0))} className="fas fa-arrow-left"></i></h2>
                            </div>
                            <div className="col-4 pt-4 text-center">
                                {images && images.length > 0? <img src={images[selectedImage].image.src}></img> : 'null'}
                            </div>
                            <div className="col-1" style={arrowDivStyle}>
                                <h2><i style={arrowStyle} onClick={() => setSelectedImage(selectedImage < images.length - 1 ? selectedImage + 1 : 0)} className="fas fa-arrow-right"></i></h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="offset-5 col-2">
                                <button onClick={() => dodajUcenika()} className="btn btn-success">Dodaj</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Classroom;