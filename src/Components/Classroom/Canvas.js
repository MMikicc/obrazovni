import React, { useEffect, useRef, useState } from 'react';
import { LoadImages } from './AssetManager';
import { BUBBLE_SORT, SELECTION_SORT } from './SortTypes';
let i=0,j=0, minIndex = 0;

const Canvas = (props) => {
    const canvasRef = useRef();
    const divRef = useRef();
    const [context, setContext] = useState();
    const [students, setStudents] = useState();
    const [linePosition, setLinePosition] = useState({});
    const [studentsSort, setStudentsSort] = useState({});

    useEffect(() => {
        i = j = minIndex = 0;
        const canvas = setupCanvas();
        const images = LoadImages();
        drawStudents(canvas.getContext('2d'), canvas, images);
        let sortStudents = [...props.students];
        sortStudents = sortStudents.sort((a, b) => {
            return a.height - b.height
        });
        setStudentsSort(sortStudents);
    }, [props.students])

    const setupCanvas = () => {
        const canvas = canvasRef.current;
        const context = canvas.getContext('2d');
        canvas.width = divRef.current.clientWidth;
        canvas.height = divRef.current.clientWidth / 2;
        setContext(context);
        return canvas;
    }

    const drawStudentName = (context, student, x, y, studentWidh) => {
        context.font = "bold 15px Arial";
        let name = student.firstname + ' ' + student.lastname[0];
        let nameWidth = context.measureText(name).width;
        let nameX = x + (studentWidh - nameWidth) / 2;
        context.fillText(name, nameX, y - 15);
        let heightLabel = student.height + ' cm';
        let heightLabelWidth = context.measureText(heightLabel).width;
        let heightLabelX = x + (studentWidh - heightLabelWidth) / 2;
        context.fillText(heightLabel, heightLabelX, y + student.height + 15)
    }

    const drawLine = (context, x, y, xWhere, yWhere) => {
        context.setLineDash([5, 3]);
        context.beginPath();
        context.moveTo(x,y);
        context.lineTo(xWhere, yWhere);
        context.stroke();
    }

    const drawStudents = (context, canvas, images) => {
        let width = canvas.width / props.students.length;
        let aspectRatio = props.students.length >= 6 ? (width / images[0].image.height) : 1;
        let newWidth;
        let x = 50;
        let y = 200;
        let tallestStudent = Math.max.apply(Math, props.students.map((student) => { return student.height }));
        let newStudents = [];
        let foundImage;
        props.students.forEach(student => {
            foundImage = images.find(image => image.name === student.image.slice(0,6));
            newWidth = (student.height * aspectRatio);
            newStudents.push({...student, x: x, y: y + tallestStudent - student.height, width: newWidth, image: foundImage.image});
            context.drawImage(foundImage.image, x, y + tallestStudent - student.height, newWidth, student.height);
            drawStudentName(context, student, x, y + tallestStudent - student.height, newWidth);
            x += newWidth + 50;
        });
        drawLine(context, 0, y+tallestStudent, canvas.width, y+tallestStudent);
        setLinePosition({x: 0, y: y+tallestStudent, xTo: canvas.width, yTo: y+tallestStudent});
        setStudents(newStudents);
    }

    const moveStudentX = (student, moveXDistance, animationSpeed, xDirection, activateSort, sortType) => {
        let x = student.x;
        let animationKey;
        let sortArray = () => {};
        if (sortType === BUBBLE_SORT) {
            sortArray = () => bubbleSort(sortType);
        }
        else if (sortType === SELECTION_SORT) {
            sortArray = () => selectionSort(sortType);
        }
        const render = () => {
            x += xDirection * animationSpeed;
            animationKey = window.requestAnimationFrame(render);
            context.clearRect(x - (animationSpeed * xDirection), student.y - 40, student.width + 20, student.height + 60);
            context.drawImage(student.image, x, student.y, student.width, student.height);
            drawLine(context, linePosition.x, linePosition.y, linePosition.xTo, linePosition.yTo);
            drawStudentName(context, student, x, student.y, student.width);
            if ( (xDirection === 1 && x >= student.x + moveXDistance) || (xDirection === -1 && x <= student.x - moveXDistance)) {
                let newStudents = students.map((s) => {
                    return student.id === s.id ? {...s, x: x} : s
                })
                setStudents(newStudents);
                student.x = x;
                moveStudent(student, 0, animationSpeed, () => {}, 'none', 'up', activateSort, sortArray, sortType);
                window.cancelAnimationFrame(animationKey);
            }

        }
        render();
    }

    const moveStudent = (student, moveXDistance, animationSpeed, moveStudentX, xDirection, yDirection, activateSort, callSort, sortType) => {
        let y = student.y;
        let animationKey;
        yDirection = yDirection === 'down'? 1 : -1;
        const render = () => {
            y += yDirection * animationSpeed;
            animationKey = window.requestAnimationFrame(render);
            context.clearRect(student.x - 15, 0, student.width + 30, 800);
            context.drawImage(student.image, student.x, y, student.width, student.height);
            drawLine(context, linePosition.x, linePosition.y, linePosition.xTo, linePosition.yTo);
            drawStudentName(context, student, student.x, y, student.width);


            if ((yDirection === 1 && y >= student.height + student.y + 70) || (yDirection === -1 && y <= student.y - student.height - 70)) {
                let newStudents = students.map((s) => {
                    return student.id === s.id ? {...s, y: y} : s
                });
                setStudents(newStudents);
                student.y = y;
                window.cancelAnimationFrame(animationKey);
                moveStudentX(student, moveXDistance, animationSpeed, xDirection === 'right'? 1 : -1, activateSort, sortType);
                setTimeout(() => {
                    callSort();
                }, 500)
            }
        }
        render();
    }

    const switchStudentsAnimation = (s1, s2, sortType) => {
        let animationSpeed = 5;
        if (s1.height > s2.height) {
            moveStudent(s1, s2.x - s1.x, animationSpeed, moveStudentX, 'right', 'down', true, () => {}, sortType);
            moveStudent(s2, s2.x - s1.x, animationSpeed, moveStudentX, 'left', 'down', false, () => {}, 'none');
        }

    }

    const bubbleSort = (sortType) => {
        if (i < students.length - 1) {
            let student1 = students[j];
            let student2 = students[j+1];
            let sort = true
            if (student1.height > student2.height) {
                swap(j, j+1);
                sort = false;
                switchStudentsAnimation(student1, student2, sortType);
                checkIfSorted();
            }
            j=j+1;
            if (j >= students.length-i-1) {
                j = 0;
                i = i + 1;
            }
            if (sort) {
                bubbleSort(sortType);
            }
        }
    }

    const selectionSort = (sortType) => {
        if (i< students.length - 1) {
            minIndex = i;            
            for (let j = i+1; j < students.length; j++) {
                if (students[j].height < students[minIndex].height) {
                    minIndex = j;
                }
            }
            if (i === minIndex) {
                i++;
                selectionSort(sortType);
            } else {
                let student1 = students[i];
                let student2 = students[minIndex];
                swap(i, minIndex);
                switchStudentsAnimation(student1, student2, sortType);
                checkIfSorted();
                i++;
            }
        }
    }

    const checkIfSorted = () => {
        let isEqual = true;
        for (let index = 0; index < students.length; index++) {
            if (students[index].id !== studentsSort[index].id) {
                isEqual = false;
                break;
            }
            
        }
        if (isEqual) {
            setTimeout(() => {
                props.disableBackground();
            }, 1500)
        }
    }

    const sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));      
    }

    async function partition (low, high) 
    { 
        let pivot = students[high];    // pivot 
        let i = (low - 1);  // Index of smaller element 
    
        for (let j = low; j <= high- 1; j++) 
        { 
            // If current element is smaller than the pivot 
            if (students[j].height < pivot.height) 
            { 
                i++;    // increment index of smaller element 
                if( i != j) {
                    swap(i, j);
                    switchStudentsAnimation(students[j], students[i], "none");
                    checkIfSorted();
                    await sleep(3000); 
                }
            } 
        } 
        if(i + 1 != high) {
            swap(i + 1, high);
            switchStudentsAnimation(students[high], students[i + 1], "none");
            checkIfSorted();
            await sleep(3000);    
        }
        return (i + 1); 
    } 

    async function quickSort(low, high) {
        if (low < high)
        {
            /* pi is partitioning index, students[pi] is now
            at right place */
            let pi = await partition(low, high);

            await quickSort(low, pi - 1);  // Before pi
            await quickSort(pi + 1, high); // After pi
        }
    }

    const swap = (a, b) => {
        const temp = students[a];
        students[a] = students[b];
        students[b] = temp;
    }

    return(
        <div style={{display: 'flex'}}>
            <div  ref={divRef} style={{width: '80%'}}>
                <canvas style={{borderLeft:'3px solid black', borderBottom: '3px solid black'}} ref={canvasRef}></canvas>
            </div>
            <div style={{width: '20%', textAlign: 'center'}}>
                <button style={{width: '50%'}} className="btn btn-success" onClick={() => {bubbleSort(BUBBLE_SORT); props.showSortInfo("Bubble"); props.disableBackground(true)}}>Bubble sort</button><br></br><br></br>
                <button style={{width: '50%'}} className="btn btn-success" onClick={() => {selectionSort(SELECTION_SORT); props.showSortInfo("Selection"); props.disableBackground(true)}}>Selection sort</button><br></br><br></br>
                <button style={{width: '50%'}} className="btn btn-success" onClick={() => {quickSort(0, students.length - 1); props.showSortInfo("Quick"); props.disableBackground(true)}}>Quick sort</button>
            </div>
        </div>
    ) 
}

export default Canvas;